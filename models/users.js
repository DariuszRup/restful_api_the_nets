var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/database');
var connection = mysql.createConnection(dbconfig.connection);

connection.query('USE ' + dbconfig.database);

var Modal={

    QuerygetAll:function(callback){

        return connection.query("Select * from users",callback);

    },
    QuerygetById:function(id,callback){

        return connection.query("select * from users where id=?",[id],callback);
    },
    Queryadd:function(Modal,callback){
        return connection.query("Insert into users values(?,?,?)",[Modal.username,Modal.username,Modal.username],callback);
    },
    QueryDelete:function(id,callback){
        return connection.query("delete from users where id=?",[id],callback);
    },
    QueryUpdate:function(id,Modal,callback){
        return connection.query("update users set username=?,password=?,role=? where id=?",[Modal.username,Modal.password,Modal.role,id],callback);
    }

};
module.exports=Modal;