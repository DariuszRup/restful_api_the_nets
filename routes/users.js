var Modal=require('../models/users');


// app/routes.js
module.exports = function(app, passport) {

    // ========= API ROUTES

    // HOME
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
        // res.send('respond with a resource');
    });


    // LOGIN
    app.post('/vl/users/login', isUnLoggedIn, passport.authenticate('login', { failWithError: true
        }),
        function(req, res) {
            // handle success
            res.json(200, { success: 'Ok' })
        },
        function(err, req, res, next) {
            // handle error
            console.log(err);
            return res.json(err.status, { error: err.message })
        }
        );

    // REGISTER
    app.post('/vl/users/register', isUnLoggedIn, passport.authenticate('register', {
        }),
        function(req, res) {
            // res.status(200);
            res.json(200, { success: 'Ok' })
        },
        function(err, req, res, next) {
            // handle error
            console.log(err);
            return res.json(err.status, { error: err.message })
        }
    );

    // LOGOUT
    app.post('/vl/users/logout', isLoggedIn, function(req, res) {
        req.logout();
        res.json(200, { success: 'Ok' })
        // res.redirect('/');
    });

    // GET USER
    app.get('/vl/users/:id?', requireRole("admin", "editor"), function(req, res) {
        if(req.params.id){

            Modal.QuerygetById(req.params.id,function(err,rows){

                if(err)
                {
                    res.json(400, { error: 'Bad Request' })
                }
                else {
                    if (rows.length == 0) {res.json(400, { error: 'Bad Request' })}
                    else {res.json({ 'username': rows[0].username, 'role': rows[0].role });}
                    // res.json(rows);
                }
            });
        }
        else {
            res.json(400, { error: 'Bad Request' })
        }
    });

    // UPDATE USER
    app.post('/vl/users/:id?', isLoggedIn, function(req, res) {

        if ((req.user.role == 'user') && (req.user.id != req.params.id))
        {
            return res.json(401, { error: 'Unauthorized' })
        }
        else if ((req.user.role == 'user') && ((req.body.role == 'editor') || (req.body.role == 'admin')))
        {
            return res.json(401, { error: 'Unauthorized' })
        }
        else if ((req.user.role == 'editor') && ((req.body.role == 'admin')))
        {
            return res.json(401, { error: 'Unauthorized' })
        }
        else
        {
        Modal.QueryUpdate(req.params.id,req.body,function(err,rows){
            if(err)
            {
                res.json(400, { error: 'Bad Request' })
            }
            else{}
            {
                if (rows.length == 0) {res.json(400, { error: 'Bad Request' })}
                else {res.json(200, { success: 'OK' });}
                // res.json(rows);
            }
        });}
    });

    // DELETE USER
    app.delete('/vl/users/:id?', requireRole("admin", "editor"), function(req, res) {

        Modal.QueryDelete(req.params.id,function(err,count){
            if(err)
            {
                res.json(400, { error: 'Bad Request' })
            }
            else
            {
                res.json(200, { success: 'OK' });
            }

        });
    });


};

// route middleware
function isLoggedIn(req, res, next) {

    if (req.headers['content-type'] != 'application/json') {
        return res.json(406, { error: 'Unauthorized' })};

    if (req.isAuthenticated())
        return next();
    return res.json(401, { error: 'Unauthorized' })
}

function isUnLoggedIn(req, res, next) {

    if (req.headers['content-type'] != 'application/json') {
        return res.json(406, { error: 'Not Acceptable' })};

    if (req.isUnauthenticated())
        return next();
    return res.json(401, { error: 'Unauthorized' })
}

function requireRole(role, role2) {
    return function (req, res, next) {

        if (req.headers['content-type'] != 'application/json') {
            return res.json(406, { error: 'Not Acceptable' })};

        if ((req.user.role === role) || (req.user.role === role2)) {
            return next();
        }
        return res.json(401, { error: 'Unauthorized' })
    }
}

function isNotAcceptable(json) {
    return function (req, res, next) {
    if (json == 'application/json')
        return next();

}}
