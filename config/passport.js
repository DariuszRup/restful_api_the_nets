// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local-roles').Strategy;

// load up the user model
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('./database');
var connection = mysql.createConnection(dbconfig.connection);

connection.query('USE ' + dbconfig.database);
// expose this function to our app using module.exports

module.exports = function(passport) {

    // passport session setup

    // serialize
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // deserialize
    passport.deserializeUser(function(id, done) {
        connection.query("SELECT * FROM users WHERE id = ? ",[id], function(err, rows){
            done(err, rows[0]);
        });
    });

    // REGISTER
    passport.use(
        'register',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            roleField: 'role',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, role, done) {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            connection.query("SELECT * FROM users WHERE username = ?",[username], function(err, rows) {
                if (err)
                    return done(err);
                if (rows.length) {
                    return done(null, false);
                } else {
                    // if there is no user with that username
                    // create the user
                    var newUserMysql = {
                        username: username,
                        password: bcrypt.hashSync(password, null, null), //  use the generateHash function in our user model
                        role: role
                    };

                    var insertQuery = "INSERT INTO users ( username, password, role ) values (?,?,?)";

                    connection.query(insertQuery,[newUserMysql.username, newUserMysql.password, newUserMysql.role],function(err, rows) {
                        newUserMysql.id = rows.insertId;

                        return done(null, newUserMysql);
                    });
                }
            });
        })
    );

    // =========================================================================
    // LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'login',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            roleField: 'role',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, role, done) { // callback with email and password from our form
            connection.query("SELECT * FROM users WHERE username = ?",[username], function(err, rows){


                if (err)
                    return done(err);

                // no user found
                if (!rows.length) {
                    return done(null, false);
                }

                // if the user is found but the password is wrong
                if (!bcrypt.compareSync(password, rows[0].password))
                    return done(null, false);

                // all is well, return successful user
                return done(null, rows[0]);
            });
        })
    );
};
